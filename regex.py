import re;

def find_all_keystring(test):
	key=re.compile(r"\bhello\sworld\b")
	return test.findall(key)
def  find_all_words(test):
	key=re.compile(r"[aeiou]{3}")
	return test.findall(key.split('\W'))
def find_all_flight(test):
	key=re.compile(r"^A{2}\d{3,4}$")
	return test.findall(key)
def test():
     a="Programmers will often write hello world as their first project with a programming language"
     b="The gooey peanut butter and jelly sandwich was a beauty."
     c="AA312 AA1298 US3194 NW314"
    print (find_all_keystring(a))
    print  (find_all_words(b))
    print (find_all_flight(c))