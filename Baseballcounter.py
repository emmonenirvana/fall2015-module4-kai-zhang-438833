import sys, os
import math
import re

class BaseStatCounter:
	def __init__(self, name, bats, hits):
		self.name = name
		self.bats = bats
		self.hits = hits
	def counter(self):
		#self.counter = round(float(self.hits) / float(self.bats), 3)
		self.counter = float(self.hits) / float(self.bats)

if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])

filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

namereg=re.compile(r"[A-Z]\w{1,} [A-Z]\w{1,10}(?=\sbatted)")
batsreg=re.compile(r"\b\d\b(?=\stimes)")
hitsreg=re.compile(r"\b\d\b(?=\shits)")

f = open(filename)

record = list()
namelist = list()

for line in f:
	line = line.strip()

	if not (line.startswith('===') or line.startswith('#') or (len(line) == 0)):	
		name = re.search(namereg, line)
		bats = re.search(batsreg, line)
		hits = re.search(hitsreg, line)

		if name:
			if name.group() not in namelist:
				player = BaseStatCounter(name.group(), int(bats.group()), int(hits.group()))
				record.append(player)
				namelist.append(name.group())
			else:
				for i, num in enumerate(record):
					if (name.group() == num.name):
						num.bats += int(bats.group())
						num.hits += int(hits.group())

f.close()

for i, num in enumerate(record):
	num.counter()

record=reversed(sorted(record,key=lambda player:player.counter))

with open("out.txt",'w') as file:
	for i, num in enumerate(record):
		num.counter = "%.3f"%num.counter
		file.write(num.name+": "+str(num.counter)+"\n")

	
